package com.shekel.ronen.rachelsongs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import java.io.IOException;


public class Main extends AppCompatActivity {

    public static final String LIST_TYPE = "rachelsongs.list_type";
    public static final String SONG_ID = "rachelsongs.song_id";

    private DBManager db_manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle("שירי רחל");

        ImageView image_view;
        image_view = (ImageView) findViewById(R.id.LogoImageView);
        image_view.setImageResource(R.drawable.rachelsongslogo);

        db_manager = new DBManager(this);
        try {
            db_manager.updateDataBase();
        } catch (IOException mIOException) {
            throw new Error("UnableToUpdateDatabase");
        }
    }

    public void open_full_song_list(View view) {
        Intent intent = new Intent(this, SongList.class);
        intent.putExtra(LIST_TYPE, "all");
        startActivity(intent);
    }

    public void open_favorite_song_list(View view) {
        Intent intent = new Intent(this, SongList.class);
        intent.putExtra(LIST_TYPE, "fav");
        startActivity(intent);
    }

    public void open_unread_song_list(View view) {
        Intent intent = new Intent(this, SongList.class);
        intent.putExtra(LIST_TYPE, "new");
        startActivity(intent);
    }

    public void open_search_in_songs(View view) {
        Intent intent = new Intent(this, SearchInSongs.class);
        startActivity(intent);
    }

    public void open_about(View view) {
        Intent intent = new Intent(this, About.class);
        startActivity(intent);
    }

}
