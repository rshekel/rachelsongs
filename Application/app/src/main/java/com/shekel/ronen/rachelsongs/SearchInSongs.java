package com.shekel.ronen.rachelsongs;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.SearchView;

import static com.shekel.ronen.rachelsongs.Main.LIST_TYPE;
import static com.shekel.ronen.rachelsongs.SongList.WORDS_QUERY;


public class SearchInSongs extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private DBManager db_manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_in_songs);
        getSupportActionBar().setTitle("חיפוש בשירים");

        SearchView searchView = (SearchView) findViewById(R.id.search_in_songs);
        searchView.setQueryHint("מילים כלשהם שאתה זוכר מתוך השיר?");

        searchView.setOnQueryTextListener(this);
    }
    public boolean onQueryTextSubmit(String query) {
        Intent intent = new Intent(this, SongList.class);
        intent.putExtra(LIST_TYPE, "query");
        intent.putExtra(WORDS_QUERY, query);
        startActivity(intent);
        return false;
    }

    public boolean onQueryTextChange(String newText) {
        // do something when text changes
        return false;
    }

}
