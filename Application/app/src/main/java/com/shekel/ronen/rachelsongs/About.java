package com.shekel.ronen.rachelsongs;

import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import java.io.IOException;
import java.io.InputStream;

import static com.shekel.ronen.rachelsongs.ShowSong.StreamToString;

/**
 * Created by ronen on 10/13/2017.
 */

public class About extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        getSupportActionBar().setTitle("אודות");

        Intent intent = getIntent();

        WebView htmlWebView = (WebView) findViewById(R.id.about_html);

        AssetManager mgr = getBaseContext().getAssets();
        try {
            InputStream in = mgr.open("about.html", AssetManager.ACCESS_BUFFER);
            String htmlContentInStringFormat = StreamToString(in);
            in.close();

            htmlWebView.loadDataWithBaseURL("file:///android_asset/", htmlContentInStringFormat, "text/html", "utf-8", null);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}

