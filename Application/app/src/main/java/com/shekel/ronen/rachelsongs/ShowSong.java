package com.shekel.ronen.rachelsongs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.CheckBox;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;


public class ShowSong extends AppCompatActivity {

    private DBManager db_manager;
    Song this_song;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_song);
        getSupportActionBar().setTitle("שיר");

        db_manager = new DBManager(this);

        Intent intent = getIntent();

        WebView htmlWebView = (WebView) findViewById(R.id.song_html);

        this_song = db_manager.getSong(Integer.parseInt(intent.getStringExtra(Main.SONG_ID)));
        htmlWebView.loadDataWithBaseURL("file:///android_asset/", this_song.getHTMLContent(), "text/html", "utf-8", null);

        /*
        // TODO: delete these 9 lines. leaving meanwhile to remember the font games...
        AssetManager mgr = getBaseContext().getAssets();
        try {
            InputStream in = mgr.open("test.html", AssetManager.ACCESS_BUFFER);
            String htmlContentInStringFormat = StreamToString(in);
            in.close();

            htmlWebView.loadDataWithBaseURL("file:///android_asset/", htmlContentInStringFormat, "text/html", "utf-8", null);

        } catch (IOException e) {
            e.printStackTrace();
        }
        */

        if (this_song.getIsFavorite() == 1) {
            CheckBox fav_checkbox = (CheckBox) findViewById(R.id.favorite_checkbox);
            fav_checkbox.setChecked(true);
        }

        if (this_song.getIsRead() == 1) {
            CheckBox rd_checkbox = (CheckBox) findViewById(R.id.read_checkbox);
            rd_checkbox.setChecked(true);
        }

    }

    public void onCheckboxClicked(View view) {

        boolean checked = ((CheckBox) view).isChecked();

        switch(view.getId()) {
            case R.id.favorite_checkbox:
                db_manager.set_favorite(this_song, (checked) ? 1 : 0);
                break;
            case R.id.read_checkbox:
                db_manager.set_read(this_song, (checked) ? 1 : 0);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return(super.onOptionsItemSelected(item));
    }

    // TODO: delete me
    public static String StreamToString(InputStream in) throws IOException {
        if(in == null) {
            return "";
        }
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } finally {
        }
        return writer.toString();
    }

}
