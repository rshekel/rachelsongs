package com.shekel.ronen.rachelsongs;

public class Song {

    private int _id;
    private int _song_id;
    private String _title;
    private String _html_content;
    private Integer _is_read;
    private Integer _is_favorite;
    private String _song_words;

    public Song(){

    }

    public Song(int id, int song_id, String name, String _html_content, String song_words){
        this._id = id;
        this._song_id = song_id;
        this._title = name;
        this._html_content = _html_content;
        this._song_words = song_words;
    }

    public Song(int id, int song_id, String title, String _html_content, int is_read, int is_favorite, String song_words){
        this._id = id;
        this._song_id = song_id;
        this._title = title;
        this._html_content = _html_content;
        this._is_read = is_read;
        this._is_favorite = is_favorite;
        this._song_words = song_words;
    }

    public int getID(){
        return this._id;
    }

    public void setID(int id){
        this._id = id;
    }

    public int getIsFavorite(){
        if (this._is_favorite == null) {
            return 0;
        }
        return this._is_favorite;
    }

    public void setIsFavorite(boolean is_favorite){
        this._is_favorite = (is_favorite) ? 1 : 0;
    }

    public void setIsFavorite(int is_favorite){
        this._is_favorite = is_favorite;
    }

    public int getIsRead(){
        if (this._is_read == null) {
            return 0;
        }
        return this._is_read;
    }

    public void setIsRead(boolean is_read){
        this._is_read = (is_read) ? 1 : 0;
    }

    public void setIsRead(int is_read){
        this._is_read = is_read;
    }

    public int getSongID(){
        return this._song_id;
    }

    public void setSongID(int song_id){
        this._song_id = song_id;
    }

    public String getTitle(){
        return this._title;
    }

    public void setTitle(String name){
        this._title = name;
    }

    public String getHTMLContent(){
        return this._html_content;
    }

    public void setHTMLContent(String html_content){
        this._html_content = html_content;
    }

    public String getSongWords() {
        return this._song_words;
    }

    public void setSongWords(String song_words) {
        this._song_words = song_words;
    }

    public String toString() {
        return this.getTitle();
    }
}