package com.shekel.ronen.rachelsongs;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class SongList extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private ListView mListView;
    private ArrayList<Song> songs_list;
    private DBManager db_manager;
    public static final String WORDS_QUERY = "rachelsongs.words_query";

    // These 4 functions are a mostly copied implementation of an abstract ListActivity that also has an action bar
    // The problem is that i want the coolness of a ListActivity, but the modernity og AppCompatActivity, and they can't work to gether
    // See: https://gunith.wordpress.com/2015/01/24/actionbaractivity-of-android-support-v7-appcompat-listactivity/
    private final class ListOnItemClickListener implements AdapterView.OnItemClickListener {

        public void onItemClick(AdapterView<?> lv, View v, int position, long id) {
            onListItemClick((ListView) lv, v, position, id);
        }
    }

    protected ListView getListView() {
        if (mListView == null) {
            mListView = (ListView) findViewById(android.R.id.list);
            mListView.setOnItemClickListener(new ListOnItemClickListener());
        }
        return mListView;
    }

    protected void setListAdapter(ListAdapter adapter) {
        getListView().setAdapter(adapter);
    }

    protected ListAdapter getListAdapter() {
        ListAdapter adapter = getListView().getAdapter();
        if (adapter instanceof HeaderViewListAdapter) {
            return ((HeaderViewListAdapter)adapter).getWrappedAdapter();
        } else {
            return adapter;
        }
    }
    protected SongAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_song_list);
        getSupportActionBar().setTitle("רשימת שירים");

        db_manager = new DBManager(this);
        songs_list = new ArrayList<Song>();

        Intent intent = getIntent();
        String list_type =  intent.getStringExtra(Main.LIST_TYPE);

        if (list_type == null || list_type.equals("all")) {
            songs_list = db_manager.getAllSongs();
        }

        else if (list_type.equals("fav")) {
            songs_list = db_manager.getFavoriteSongs();
        }

        else if (list_type.equals("new")) {
            songs_list = db_manager.getNotReadSongs();
        }

        else if (list_type.equals("query")) {
            String query = intent.getStringExtra(WORDS_QUERY);
            songs_list = db_manager.getSongsWithWords(query);
        }

        adapter = new SongAdapter(this,
                R.layout.song_in_list, songs_list);

        setListAdapter(adapter);
    }

    protected void onListItemClick(ListView list, View view, int position, long id)
     {
        Song selected_song = (Song)getListView().getItemAtPosition(position);
        Intent intent = new Intent(this, ShowSong.class);
        intent.putExtra(Main.SONG_ID, String.format("%03d", selected_song.getID()));
        startActivity(intent);
      }

    // and thank these guys for doing the searching in SongList:
    // https://github.com/erangaeb/dev-notes/tree/master/android-search-list
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    public boolean onQueryTextChange(String newText) {
        // SongAdapter is an arrayAdapter which defaults an implementation of getFilter
        // which filters by prefix - which is just fine for me :)
        adapter.getFilter().filter(newText);

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_with_search, menu);

        SearchManager searchManager = (SearchManager)getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchMenuItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) searchMenuItem.getActionView();

        ComponentName componentName = getComponentName();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName));
        searchView.setOnQueryTextListener(this);
        searchView.setIconifiedByDefault(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return(super.onOptionsItemSelected(item));
    }

    }
