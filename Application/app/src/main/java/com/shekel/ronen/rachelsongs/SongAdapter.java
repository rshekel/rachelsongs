package com.shekel.ronen.rachelsongs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;


class SongAdapter extends ArrayAdapter<Song> implements Filterable{
    private ArrayList<Song> items;

    public SongAdapter(Context context, int textViewResourceId, ArrayList<Song> items) {
        super(context, textViewResourceId, items);
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Song song = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.song_in_list, parent, false);
        }

        TextView song_title = (TextView) convertView.findViewById(R.id.songTitle);
        song_title.setText(song.getTitle());
        return convertView;
    }
}