package com.shekel.ronen.rachelsongs;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public class DBManager extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 8;

    private static final String DATABASE_NAME = "songs_db";
    private static String DATABASE_PATH = "";
    private SQLiteDatabase mDataBase;

    private final Context mContext;
    private boolean mNeedUpdate = false;

    private static final String TABLE_SONGS = "songs";

    private static final String KEY_ID = "_id";
    private static final String KEY_SONG_ID = "song_id";
    private static final String KEY_TITLE = "title";
    private static final String KEY_HTML_CONTENT = "html_content";
    private static final String KEY_IS_READ = "is_read";
    private static final String KEY_IS_FAVORITE = "is_favorite";
    private static final String KEY_SONG_WORDS = "song_words";

    public DBManager(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        if (android.os.Build.VERSION.SDK_INT >= 17)
            DATABASE_PATH = context.getApplicationInfo().dataDir + "/databases/";
        else
            DATABASE_PATH = "/data/data/" + context.getPackageName() + "/databases/";
        this.mContext = context;

        assertDataBase();

        this.getReadableDatabase();

    }

    public void updateDataBase() throws IOException {
        if (mNeedUpdate) {
            File dbFile = new File(DATABASE_PATH + DATABASE_NAME);
            if (dbFile.exists())
                dbFile.delete();

            assertDataBase();

            mNeedUpdate = false;
        }
    }

    private void assertDataBase() {
        File dbFile = new File(DATABASE_PATH + DATABASE_NAME);
        if (!dbFile.exists()) {
            this.getReadableDatabase();
            this.close();
            try {
                copyDBFile();
            } catch (IOException mIOException) {
                throw new Error("ErrorCopyingDataBase");
            }
        }
    }

    private void copyDBFile() throws IOException {
        InputStream mInput = mContext.getAssets().open(DATABASE_NAME);
        //InputStream mInput = mContext.getResources().openRawResource(R.raw.info);
        OutputStream mOutput = new FileOutputStream(DATABASE_PATH + DATABASE_NAME);
        byte[] mBuffer = new byte[1024];
        int mLength;
        while ((mLength = mInput.read(mBuffer)) > 0)
            mOutput.write(mBuffer, 0, mLength);
        mOutput.flush();
        mOutput.close();
        mInput.close();
    }

    public boolean openDataBase() throws SQLException {
        mDataBase = SQLiteDatabase.openDatabase(DATABASE_PATH + DATABASE_NAME, null, SQLiteDatabase.CREATE_IF_NECESSARY);
        return mDataBase != null;
    }

    @Override
    public synchronized void close() {
        if (mDataBase != null)
            mDataBase.close();
        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion > oldVersion)
            mNeedUpdate = true;
    }

    Song getSong(int id) {

        String normalized_id = normalizeID(id);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_SONGS, new String[]{KEY_ID, KEY_SONG_ID,
                        KEY_TITLE, KEY_HTML_CONTENT, KEY_IS_READ, KEY_IS_FAVORITE, KEY_SONG_WORDS}, KEY_ID + "=?",
                new String[]{normalized_id}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Song song = new Song(Integer.parseInt(cursor.getString(0)), Integer.parseInt(cursor.getString(1)),
                cursor.getString(2), cursor.getString(3), cursor.getInt(4), cursor.getInt(5), cursor.getString(6));
        return song;
    }

    public ArrayList<Song> getFavoriteSongs() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Song> songList = new ArrayList<Song>();

        Cursor cursor = db.query(TABLE_SONGS, new String[]{KEY_ID, KEY_SONG_ID,
                        KEY_TITLE, KEY_HTML_CONTENT, KEY_IS_READ, KEY_IS_FAVORITE, KEY_SONG_WORDS}, KEY_IS_FAVORITE + "=?",
                new String[]{"1"}, null, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            do {
                Song song = new Song(Integer.parseInt(cursor.getString(0)), Integer.parseInt(cursor.getString(1)),
                        cursor.getString(2), cursor.getString(3), cursor.getInt(4), cursor.getInt(5), cursor.getString(6));
                songList.add(song);
            } while (cursor.moveToNext());
        }
        return songList;
    }

    public ArrayList<Song> getNotReadSongs() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Song> songList = new ArrayList<Song>();

        Cursor cursor = db.query(TABLE_SONGS, new String[]{KEY_ID, KEY_SONG_ID,
                        KEY_TITLE, KEY_HTML_CONTENT, KEY_IS_READ, KEY_IS_FAVORITE, KEY_SONG_WORDS}, KEY_IS_READ + "=?",
                new String[]{"0"}, null, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            do {
                Song song = new Song(Integer.parseInt(cursor.getString(0)), Integer.parseInt(cursor.getString(1)),
                        cursor.getString(2), cursor.getString(3), cursor.getInt(4), cursor.getInt(5), cursor.getString(6));
                songList.add(song);
            } while (cursor.moveToNext());
        }
        return songList;
    }

    public ArrayList<Song> getSongsWithWords(String query) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Song> songList = new ArrayList<Song>();

        Cursor cursor = db.query(true, TABLE_SONGS, new String[] {KEY_ID, KEY_SONG_ID,
                KEY_TITLE, KEY_HTML_CONTENT, KEY_IS_READ, KEY_IS_FAVORITE, KEY_SONG_WORDS}, KEY_SONG_WORDS + " LIKE ?",
                new String[] {"%"+ query + "%" }, null, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            do {
                Song song = new Song(Integer.parseInt(cursor.getString(0)), Integer.parseInt(cursor.getString(1)),
                        cursor.getString(2), cursor.getString(3), cursor.getInt(4), cursor.getInt(5), cursor.getString(6));
                songList.add(song);
            } while (cursor.moveToNext());
        }
        return songList;
    }

    public ArrayList<Song> getAllSongs() {
        ArrayList<Song> songList = new ArrayList<Song>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_SONGS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Song song = new Song();
                song.setID(Integer.parseInt(cursor.getString(0)));
                song.setSongID(Integer.parseInt(cursor.getString(1)));
                song.setTitle(cursor.getString(2));
                song.setHTMLContent(cursor.getString(3));
                song.setIsRead(cursor.getInt(4));
                song.setIsFavorite(cursor.getInt(5));
                song.setSongWords(cursor.getString(6));
                // Adding song to list
                songList.add(song);
            } while (cursor.moveToNext());
        }

        return songList;
    }

    public int set_favorite(Song song, int is_favorite) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_IS_FAVORITE, is_favorite);

        // updating row
        int count = db.update(TABLE_SONGS, values, KEY_ID + " = ?",
                new String[]{String.valueOf(song.getID())});
        Song s = getSong(song.getID());
        return count;
    }

    public int set_read(Song song, int is_read) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_IS_READ, is_read);

        // updating row
        return db.update(TABLE_SONGS, values, KEY_ID + " = ?",
                new String[]{String.valueOf(song.getID())});
    }

    public void deleteSong(Song song) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_SONGS, KEY_ID + " = ?",
                new String[]{String.valueOf(song.getID())});
        db.close();
    }

    public int getSongsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_SONGS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

    public String normalizeID(int id) {
        return String.format("%03d", id);
    }

}