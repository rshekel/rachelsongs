# For Declaration
from sqlalchemy import Column, Integer, UnicodeText, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, desc

# For Session
from sqlalchemy.orm import sessionmaker, scoped_session

Base = declarative_base()


class Song(Base):
    __tablename__ = 'songs'
    _id = Column(Integer, primary_key=True)
    song_id = Column(Integer)
    title = Column(UnicodeText)
    html_content = Column(UnicodeText)
    is_read = Column(Integer, default=0)
    is_favorite = Column(Integer, default=0)
    song_words = Column(UnicodeText)
    

def create_db():
    engine = create_engine('sqlite:///rachel_songs.db')
    Base.metadata.create_all(engine)


# Bind the engine to the metadata of the Base class so that the
# declaratives can be accessed through a DBSession instance

engine = create_engine('sqlite:///rachel_songs.db')
Base.metadata.bind = engine
DBSession = scoped_session(sessionmaker(bind=engine))
