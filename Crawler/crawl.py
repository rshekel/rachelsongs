import os
import re
import requests 
import lxml.html as hx
from lxml import etree 
import unicodedata

from rachel_db import create_db, DBSession, Song 

BASE_RACHEL_URL = "http://benyehuda.org/rachel"
SONG_URL_FORMAT = BASE_RACHEL_URL + "/Rac%s.html"


def extract_heb_words(html):
    '''
        This function extracts the actual letters of song so it will be easily searchable. 
        We take all the hebrew tags, and take only letters (no dots), and turn all whitespace to spaces - because that is what makes sense querying.  
    '''

    # If parsing html yourself, use this re. 
    # Get hebrew unicode characters 1488-1514, according to this: https://en.wikipedia.org/wiki/Unicode_and_HTML_for_the_Hebrew_alphabet    
    # heb_letters_and_whitespace = re.findall("&#148[8,9];|&#149[0-9];|&#150[0-9];|&#151[0-4]|[\s.,;];", line)
 
    
    final_string = ""
    tree = etree.fromstring(html, etree.HTMLParser())
    for elem in tree.getiterator():
        if elem.tag == 'span' and elem.get("lang") == "HE" and elem.text:
            line = unicodedata.normalize("NFKD", unicode(elem.text)) 
            letters_and_whitespace = "".join(re.findall(ur'[\u05d0-\u05ea\s.,;-]', line))
            final_string += letters_and_whitespace + " "
 
    # remove \r\n and stuff and have space instead 
    final_string = " ".join(final_string.split())

    return final_string

def remove_by_headers(content):
    by_start_head = content.find('<!-- begin BY head -->')
    by_end_head = content.find('<!-- end BY head -->') + len('<!-- end BY head -->')
    by_start_body = content.find('<!-- begin BY body -->')
    by_end_body = content.find('<!-- end BY body -->') + len('<!-- end BY body -->')
    
    return content[:by_start_head] + content[by_end_head:by_start_body] + content[by_end_body:]

def get_song(song_id):
    song_id = str(song_id).zfill(3)
    url = SONG_URL_FORMAT % song_id
    content = requests.get(url).content
    title = hx.fromstring(content).find(".//title").text
    title = " ".join(title.split()).replace("/", "--").replace("*", "-").replace("\"", '').replace(":", "-") # For opening file with the name of title it can't have \r\n or / or *  or "
    html_content = remove_by_headers(content).decode("utf-8")
    song_words = extract_heb_words(html_content)
    s = Song(song_id=song_id, title=title, html_content=html_content, song_words=song_words)
    return s
    
    
def crawl_to_db():
    if os.path.isfile('rachel_songs.db'):
        print("Deleting old db")
        os.remove('rachel_songs.db')

    create_db()
    session = DBSession()
   
    for i in range(144):
        s = get_song(i + 1)
        session.add(s)
        session.commit()
        del s 
        
def crawl_to_files(with_song_names=False):  
    for i in range(144):
        s = get_song(i + 1)
        if with_song_names:
            file_path = u"./Songs/{song_id}-{song_name}.html".format(song_id=unicode(i + 1).zfill(3), song_name=s.title)
        else:
            file_path = u"./SongsIDs/{song_id}.html".format(song_id=unicode(i + 1).zfill(3))
        f = open(file_path, "wb")
        f.write(s.html_content.encode("utf-8"))
 
if __name__ == '__main__':
    crawl_to_db()
